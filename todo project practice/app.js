document.querySelector(".button").addEventListener("click", Data);

// post method
function Data(e) {
  var input = document.getElementById("number").value;
  console.log(input);
  e.preventDefault();
}

function getData(callback) {
  // const number = document.querySelector('input[type= "number"]').value;

  // create xhr object
  const xhr = new XMLHttpRequest();
  // open
  xhr.open("GET", `https://jsonplaceholder.typicode.com/todos`, true);
  xhr.onload = function() {
    var data = this.responseText;
    var jsonResponse = JSON.parse(data);
    callback(jsonResponse);
  };
  xhr.send();
}

function showData(jsonResponse) {
  var ul = document.querySelector("ul");
  console.log(ul);

  for (let i = 0; i < jsonResponse.length; i++) {
    // create li element
    var li = document.createElement("li");
    // add class
    li.className = "list-item";
    //  append li to ul
    ul.appendChild(li);
    // create new div
    var div = document.createElement("div");
    // add class to div
    div.className = "flex";
    // append div to li
    li.appendChild(div);
    // create inner div
    var div1 = document.createElement("div");
    // add class to inner div
    div1.className = "list-block";
    div1.innerText = jsonResponse[i].title;
    // create another div
    var div2 = document.createElement("div");
    // add class
    // div2.className = "botton-block";
    // append
    div.appendChild(div2);
    button = document.createElement("button");
    button.className = "delete-button button-primary";
    button.innerText = "Delete";
    div2.appendChild(button);

    // div1 append to div
    div.appendChild(div1);
  }
}
getData(showData);
