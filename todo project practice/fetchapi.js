document.getElementById("button1").addEventListener("click", getText);

document.getElementById("button2").addEventListener("click", getJson);

document.getElementById("button3").addEventListener("click", getExternal);

function getText() {
  fetch("test.txt")
    .then(function(res) {
      return res.text();
    })
    .then(function(data) {
      console.log(data);
      document.getElementById("output").innerHTML = data;
    });
}

// Get json data
function getJson() {
  fetch("get.Json")
    .then(function(res) {
      return res.json();
    })
    .then(function(data) {
      console.log(data);
      let output = "";
      data.forEach(function(post) {
        output += `<li>${post.body}</li>`;
      });
      document.getElementById("output").innerHTML = output;
    });
}

// get data from external source
function getExternal() {
  fetch("https://api.github.com/users")
    .then(function(res) {
      return res.json();
    })
    .then(function(data) {
      console.log(data);
      let output = "";
      data.forEach(function(post) {
        output += `<li>${post.id}</li>`;
      });
      document.getElementById("output").innerHTML = data;
    });
}
