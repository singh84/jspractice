// function Employee() {}

// Employee.prototype.firstName = "Arjinder";
// Employee.prototype.lastName = "Singh";
// Employee.prototype.age = "26";
// Employee.prototype.signedNDA = true;
// Employee.prototype.startDate = new Date();
// Employee.prototype.fullName = function() {
//   console.log(this.firstName + " " + this.lastName);
// };

// var student = new Employee();
// console.log(student.firstName);
// console.log(student.lastName);
// console.log(student.age);
// console.log(student.signedNDA);
// console.log(student.startDate);
// console.log(student.fullName());

function Employee(firstName, lastName, age, gender, fullName, todayDate) {
  this.firstName = "Arjinder";
  this.lastName = "Singh";
  this.age = "26";
  this.gender = "Male";
  this.todayDate = new Date();
  this.fullName = function() {
    console.log(this.firstName + " " + this.lastName);
  };
}

var student = new Employee();
console.log(student.firstName);
console.log(student.lastName);
console.log(student.age);
console.log(student.gender);
console.log(student.fullName());
console.log(student.todayDate);
