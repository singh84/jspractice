// const student = { id: "AVFD654" };
// try {
//   if (student.id) {
//     throw "student have id here";
//   }
// } catch (e) {
//   console.log(e);
//   console.log(student.id);
//   // console.log(e instanceof TypeError);
// }

// console.log("programme continue...");

// Regular expression
// let re;
// re = /hello friend/;
// // re = /hardwork/i;

// const result = re.exec("Hi Guys how are you, hello friend where are you man");
// console.log(result);

// test()- return true or false
// const result = re.test("Hello friend are you here");
// console.log(result);

// match()- Result array or null
// const pattern = "The key to succes is hardwork and patience";
// const result = pattern.match(re);
// console.log(result);

// search()- return index of first match if not found return -1
// const pattern = "The key to succes is hardwork and patience";
// const result = pattern.search(re);
// console.log(result);

// // replace()- Return new string with some or all matches of a pattern
// const pattern = "hardwork increase your personality and health";
// const result = pattern.replace(re, "Smile");
// console.log(result);

// Regular expression matacharacter symbols
let re;
// literal characters
// re = /workout/;
// re = /workout/i;

// Metacharacter Symbols
// re = /^w/i;
// re = /time$/i;
// re = /^workout time$/i;
re = /^w.rkout time$/;

// string to match
const pattern = "w.rkout time";

// Log Results
const result = re.exec(pattern);
console.log(result);

function retest(re, pattern) {
  if (re.test(pattern)) {
    console.log(`${pattern} matches ${re.source}`);
  } else {
    console.log(`${pattern} does not match ${re.source}`);
  }
}

retest(re, pattern);
