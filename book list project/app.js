// Book constuctor function
function Book(title, author, isbn) {
  this.title = title;
  this.author = author;
  this.isbn = isbn;
}
//  UI costructor
function UI() {}

// Add Book To List

UI.prototype.addBookToList = function(book) {
  const list = document.getElementById("book-list");
  //  create tr element
  const row = document.createElement("tr");
  // Insert cols
  row.innerHTML = `
<td>${book.title}</td>
<td>${book.author}</td>
<td>${book.isbn}</td>
<td><a href="#" class = "delete">X</a></td>
`;

  list.appendChild(row);
};

// show alert
UI.prototype.showAlert = function(message, className) {
  //  Crete div
  const div = document.createElement("div");
  // Add classes
  div.className = `alert ${className}`;

  // add text
  div.appendChild(document.createTextNode(message));
  // Get parent
  const container = document.querySelector(".container");
  // Get form
  const form = document.querySelector("#book-form");
  // insert alert
  container.insertBefore(div, form);
  // timeout afer 3 sec
  setTimeout(function() {
    document.querySelector(".alert").remove();
  }, 3000);
};

// delete book
UI.prototype.deleteBook = function(target) {
  if (target.className === "delete") {
    target.parentElement.parentElement.remove();
  }
};
// clear fields
UI.prototype.clearFields = function() {
  document.getElementById("title").value = "";
  document.getElementById("author").value = "";
  document.getElementById("isbn").value = "";
};
// Evenmt Listeners
document.getElementById("book-form").addEventListener("submit", function(e) {
  // Get form values
  title = document.getElementById("title").value;
  author = document.getElementById("author").value;
  isbn = document.getElementById("isbn").value;

  // Instantiate book
  const book = new Book(title, author, isbn);

  // Instantiate UI
  const ui = new UI();

  // validate
  if (title === "" || author === "" || isbn === "") {
    // alert("failed");

    // error alert
    ui.showAlert("please fill all the fields", "error");
  } else {
    // Add book to list
    ui.addBookToList(book);
    // console.log(book);

    // show success
    ui.showAlert("Book Added", "success");

    // clear field
    ui.clearFields();
  }

  e.preventDefault();
});

// Event listener for delete
document.getElementById("book-list").addEventListener("click", function(e) {
  // instantiate book
  ui.deleteBook(e.target);

  // show message
  ui.showAlert("Booked Removed!", "success");

  e.preventDefault();
});
