// document.getElementById("button").onclick = () => {
//   const axios = window.axios;
//   axios
//     .get("https://jsonplaceholder.typicode.com/posts/1")
//     .then(function(response) {
//       // handle success
//       console.log(response);
//     })
//     .catch(function(error) {
//       // handle error
//       console.log(error);
//     });
// };

// post request data
// var post = [
//   { id: "2321", body: "Look  at this post", title: "post item" },
//   { id: "452", body: "Look  at this window", title: "new post here" }
// ];
axios
  .post("https://jsonplaceholder.typicode.com/posts", {
    firstName: "Fred",
    lastName: "Flintstone"
  })
  .then(function(response) {
    console.log(response);
    let output = "";
    response.forEach(function(response) {
      output += `<li>${response}</li>`;
    });
    document.getElementById("output").innerHTML = output;
    console.log(output);
  })
  .catch(function(error) {
    console.log(error);
  });
