// getdata method start here

async function getExternal() {
  try {
    let response = await axios.get(
      "https://jsonplaceholder.typicode.com/posts"
    );
    // console.log(response);
    // let res = await response.json();
    console.log("print response", response);
    let output = "";
    response.data.forEach(function(newItem) {
      output += `<li class="list-item">
            <div class="flex">
              <div class="get-data">
                <ul class="inner-list">
                  <li class="inner-getlist">${newItem.title}</li>
                </ul>
              </div>

              <button class="button-style" onclick="deletePost(event,${newItem.id})">X</button>

              <button class="button-primary" onclick="editPost(${newItem.title},${newItem.body})">Edit</button>
            </div>
          </li>`;
    });

    // console.log("print output value", output);
    // var elem = (document.getElementsByClassName(
    //   "main-list"
    // )[0].innerHTML = output);
    var result = (document.getElementsByClassName(
      "main-list"
    )[0].innerHTML = output);
    console.log("this is result by class", result);
    // var final = (document.getElementById("main-list").innerHTML = output);
    // console.log("this final output by id", final);

    // document.getElementById("main-list").innerHTML = output;
  } catch (error) {
    console.error(error);
  }

  // console.log(output);

  // .catch(function(error) {
  //   console.log(error);
  // });
}

// target function call
function deletePost(e, id) {
  console.log("let see what event show", e.target);
  // console.log("this show which button is pressed", id);
  axios
    .delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
    .then(function(res) {
      console.log(res);

      if (res.status === 200) {
        var target = document.querySelector(".button-style");
        target.parentElement.parentElement.remove();
      }
    });
}

// edit function call
function editPost(title, body) {
  var newtitle = (document.getElementById("title").value = title);
  console.log("input value printed", newtitle);
  document.getElementById("description").value = body;
}

// function call
getExternal();
// get data method end here

// post method start from here
document.querySelector(".submit-button").addEventListener("click", postData);

function postData(e) {
  e.preventDefault();
  var input = document.getElementById("title").value;
  var body = document.getElementById("description").value;
  document.getElementById("image").style.display = "block";
  postMethod(input, body);

  function postMethod(input, body) {
    axios
      .post("https://jsonplaceholder.typicode.com/posts", {
        title: `${input}`,
        body: `${body}`
      })
      .then(function(res) {
        console.log("this is post response", res);
        document.getElementById("image").style.display = "none";
        getExternal();
        // let output = "";
        // res.data.forEach(function(listItem) {
        //   output += ` <li>${listItem.title}</li>`;
        // });

        // var elem = (document.getElementsByClassName(
        //   "main-list"
        // )[0].innerHTML = output);

        // console.log(output);
      });
    // .catch(function(error) {
    //   console.log(error);
    // });
  }
}

// //  data type value
// const fruit = ["Mango", "Orange", "Grapes", "Banana", "Guava"];
// fruit.push("pineapple");
// // for (i = 0; i < fruit.length; i++) {
// console.log("fruit array", fruit);

// console.log("this is string text");
// var Employee = {
//   id: "24552",
//   profile: fruit,
//   timePeriod: "fulltime"
// };

// var myCar = new Object();
// myCar.make = fruit;
// myCar.model = "Mustang";
// myCar.year = 1969;
// myCar.customer = Employee;

// console.log(myCar.customer.profile[4]);

// let x = 6;
// let y = 5;
// console.log(sum(x, y));
// function sum(a, b) {
//   return a + b;
// }

// var student = {
//   name: "Jatinder",
//   height: "170 cm",
//   class: "12th"
// };
// console.log(student.class);
