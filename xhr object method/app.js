document.getElementById("button").addEventListener("click", loadData);

function loadData() {
  // create an XHR object
  const xhr = new XMLHttpRequest();

  // OPEN
  xhr.open("GET", "https://jsonplaceholder.typicode.com/todos", true);

  // xhr.onload() = function() {
  //   if (this.status === 200) {
  //     console.log(this.responseText);
  //   }
  // };

  xhr.onload = function() {
    var data = xhr.responseText;
    if (this.status === 200) {
      var jsonResponse = JSON.parse(this.responseText);
      console.log(jsonResponse.length);
      // document.getElementById("output").innerHTML = `<h3>${JSON.parse(
      //   this.responseText
      // )}</h3>`;
      // document.getElementById("output").style.color = "green";
    }
  };

  xhr.send();
}
