// create constructor
// function Person(name, age, dob) {
//   this.name = name;
//   this.age = age;
//   this.birthday = new Date(dob);
//   this.calculateAge = function() {
//     const diff = Date.now() - this.birthday.getTime();
//     const ageDate = new Date(diff);
//     return Math.abs(ageDate.getUTCFullYear() - 1970);
//   };
// }

// const brad = new Person("john", 30, "02-23-1990");
// console.log(brad.calculateAge());

//  string
// const name1 = "Jeff";
// const name2 = new String("Jeff");
// console.log(typeof name2);

// if (name2 === "Jeff") {
//   console.log("YES");
// } else {
//   console.log("NO");
// }

// // Number
// const num1 = 5;
// const num2 = new Number(5);
// console.log(typeof num1);
// console.log(typeof num2);

// // Boolean
// const bool1 = true;
// const bool2 = new Boolean(true);
// console.log(typeof bool1);
// console.log(typeof bool2);

// // function
// const getSum1 = function(x, y) {
//   return x + y;
// };
// console.log(getSum1(10, 10));

// const getSum2 = new Function("x", "y", "return 10 + 5");
// console.log(getSum2(10, 5));

// // object
// const john1 = { name: "john" };
// const john2 = new Object({ name: "john" });
// console.log(john2);

// // Array
// const arr1 = [1, 2, 3, 4];
// const arr2 = new Array(1, 2, 3, 4);
// console.log(arr2);

// constructor prototype

function Person(firstName, lastName, dob) {
  this.firstName = firstName;
  this.lastName = lastName;
  // this.age = age;
  this.birthday = new Date(dob);
}

// calculate image
Person.prototype.calculateAge = function() {
  const diff = Date.now() - this.birthday.getTime();
  const ageDate = new Date(diff);
  return Math.abs(ageDate.getUTCFullYear() - 1970);
};

// Get full name
Person.prototype.getFullName = function() {
  return `${this.firstName} ${this.lastName}`;
};

// Get Married
Person.prototype.getsMaried = function(newLastName) {
  this.lastName = newLastName;
};
const john = new Person("john", 30, "02-23-1990");
const mary = new Person("Mary", "Johnson", "March 20 1978");
console.log(mary);

console.log(john.calculateAge());

console.log(mary.getFullName());

console.log(mary.getsMaried());
