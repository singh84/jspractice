// Game values
let min = 1,
  max = 10,
  winningNum = 2,
  guessesLeft = 3;

// UI Elements
const game = document.querySelector("#game"),
  minNum = document.querySelector(".min-num"),
  maxNum = document.querySelector(".max-num"),
  guessBtn = document.querySelector("#guess-btn"),
  guessInput = document.querySelector("#guess-input"),
  message = document.querySelector(".message");

//  Assign min and max

minNum.textContent = min;
maxNum.textContent = max;

// listen for guess
guessBtn.addEventListener("click", function() {
  let guess = parseFloat(guessInput.value);

  // validate
  if (isNaN(guess) || guess < min || guess > max) {
    setMessage(`please enter a number between ${min} and ${max}`, "green");
  }

  //    check if won
  if (guess === winningNum) {
    gameOver(true, `${winningNum} is correct, YOU WIN!`);
    //     // disable input
    //     guessInput.disabled = true;

    //     // change border color
    //     guessInput.style.borderColor = "green";

    //     // Set message
    //     setMessage(`${winningNum} is correct, YOU WIN`, "green");
  } else {
    //   wrong number
    guessesLeft -= 1;

    if (guessLeft === 0) {
      //  Game over - lost

      gameOver(
        false,
        `Game over, you lost. The correct number was ${winningNum}`
      );
      // Disable input
      //  guessInput.disabled = true;
      //  // change border color
      //  guessInput.style.borderColor = "red";
      //  // set message
      //  setMessage(
      //    `Game Over, you lost. The correct number was ${winningNum}`,
      //    "red"
    } else {
      // Game continue - answer wrong

      // change border
      guessInput.style.borderColor = "red";

      // clear Input
      guessInput.value = "";

      // tell user its the wrong number
      setMessage(`${guess} is not correct, ${guessesLeft} guesses left`, "red");
    }
  }
});

// Game over
function gameOver(won, msg) {
  let color;
  won === true ? (color = "green") : (color = "red");
  // Disable input
  guessInput.disabled = true;
  // change border color
  guessInput.style.borderColor = "color";
  //  Set text color
  message.style.color = color;
  // set message
  setMessage(msg, color);
}

//  set message
function setMessage(msg, color) {
  message.style.color = color;
  message.textContent = msg;
}
